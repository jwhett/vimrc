My VIMRC Setup
==============

The Installers
--------------
The ``*`` in this case just means that the usage applies
to both scripts.

.. code::

    Usage: jwhet-vimrc-*.sh [-h|-d custom_install_dir]
    -h -- Print this help message
    -d -- Defaults to ~/etc. New path will be created if it does not exist.

The Plugins
-----------
- tabular
- neco-ghc
- nerdtree
- tlib_vim
- syntastic
- vim-fugitive
- vim-snipmate
- vim-snippets
- vim-surround
- vim-powerline
- vim-easymotion
- haskellmode-vim
- neocomplete.vim
- vim-addon-mw-utils
- vim-colors-solarized

Reporting Issues
----------------
Please submit issues as you find problems and I will address
them as soon as possible.

Credits
-------
Thank you to all the plugin developers! This repo is mostly
so I can keep track of my own git setup and still track each
plugin repo individually.
